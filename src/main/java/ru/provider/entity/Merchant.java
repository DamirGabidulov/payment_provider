package ru.provider.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.util.StringUtils;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Table("merchants")
public class Merchant implements Persistable<String> {
    @Id
    private String id;
    @Column("secret_key")
    private String secretKey;

    @Transient
    @ToString.Exclude
    private List<Account> accounts;

    @Override
    public boolean isNew() {
        return !StringUtils.hasText(id);
    }
}
