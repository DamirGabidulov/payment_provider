package ru.provider.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Table("accounts")
public class Account implements Persistable<String> {
    @Id
    private String id;
    @Column("amount")
    private BigDecimal amount;
    @Column("currency")
    private Currency currency;
    @Column("language")
    private Language language;

    @Transient
    private Merchant merchant;
    @Column("merchant_uid")
    private String merchantUid;

    @Transient
    @ToString.Exclude
    private List<Transaction> transactions;

    @Override
    public boolean isNew() {
        return !StringUtils.hasText(id);
    }
}
