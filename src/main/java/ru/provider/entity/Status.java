package ru.provider.entity;

public enum Status {

    IN_PROGRESS, SUCCESS, FAILED
}
