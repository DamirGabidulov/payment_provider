CREATE TYPE currency_type as ENUM('usd', 'eur', 'rub');
CREATE TYPE language_type as ENUM('en', 'ru');