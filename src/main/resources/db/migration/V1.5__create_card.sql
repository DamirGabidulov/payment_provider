CREATE TABLE IF NOT EXISTS cards(
    id BIGSERIAL PRIMARY KEY,
    card_number BIGINT,
    expiration_date VARCHAR(8),
    cvv INTEGER,
    customer_uid VARCHAR(36),
    CONSTRAINT fk_cards_customers
    FOREIGN KEY (customer_uid) REFERENCES customers(id)
);