CREATE TABLE IF NOT EXISTS merchants(
    id VARCHAR(36) PRIMARY KEY DEFAULT gen_random_uuid(),
    secret_key varchar(36)
);

ALTER TABLE accounts ADD CONSTRAINT fk_accounts_merchants
FOREIGN KEY (merchant_uid) REFERENCES merchants(id);